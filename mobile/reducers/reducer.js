export const GET_CATEGORIES = 'GET_CATEGORIES_REQUEST';

const initialState = {
    repos: []
};

export default function reducer(state = initialState, action) {
    // const data = action.payload;
    switch (action.type) {
        case GET_CATEGORIES:
            return { 
                ...state, 
                repos: ['Sample', "Test", "Some"] 
            };
        default:
            return state;
    }
}

export function listCategories() {
    return {
        type: GET_CATEGORIES,
        payload: { }
    };
}

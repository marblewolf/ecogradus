import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class SettingsScreen extends React.Component {
    render() {
        return (
          <View style={{padding: 25}}>
            <Text>Ім&apos;я: </Text>
            <View style={styles.textContainer}>
                <Text style={styles.text}>Андрій</Text>
            </View>
            <Text>Прізвище: </Text>
            <View style={styles.textContainer}>
                <Text style={styles.text}>Кравчук</Text>
            </View>     
            <Text>Дата реєстрації: </Text>    
            <View style={styles.textContainer}>
                <Text style={styles.text}>15.11.2019</Text>
            </View>  
         </View>
        );
      }
    }

  
const styles = StyleSheet.create({
   boxed: {
        marginHorizontal: 10,
        marginVertical: 5,
        flex: 1
    },
    textContainer: {
        display: 'flex',
        alignItems: "center",
        padding: 5,
        marginBottom: 10,
        justifyContent: "center",
        fontSize: 100,
        borderColor: "black",
        borderRadius: 4,
        borderWidth: 1,
        borderStyle: "solid",
    },
    checkboxContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
      textAlign: "center"  
    },
    boxedButton: {
        marginHorizontal: 10,
        marginVertical: 5
    },
    button: {
        padding: 10
    }
});

import React from 'react';
import {
    Button,
    Text,
    View,
    StyleSheet,
    FlatList,
    CheckBox,
    Platform,
    AsyncStorage
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';

import data from './../data/categories';

export default class HabitScreen extends React.Component {
      

    constructor(props) {
        super(props);
        const { route } = this.props;
        const categoryId = parseInt(route.params['categoryId']);
        const habitId = parseInt(route.params['habitId']);
        this.habit = data.find(x => x.id === categoryId).habits.find(x => x.id === habitId);
        this.state = {
            checkedDays: [
                [true, 'Понеділок'],
                [true, 'Вівторок'],
                [true, 'Середа'],
                [true, 'Четвер'],
                [true, `П'ятниця`],
                [true, 'Субота'],
                [true, 'Неділя'],
            ],
            date: new Date(Date.now() + 1000 * 3600),
            mode: 'date',
            show: false,
        }; 
        this.onSubmit = this.onSubmit.bind(this);
    }

    render() {
        const habit = this.habit;
        const { show, date, mode } = this.state;


        return (
            <>
                <View style={styles.boxed}>
                    <Text>Назва: </Text>
                    <View style={styles.textContainer}>
                        <Text style={styles.text}>{habit.name}</Text>
                    </View>
                    <Text>Опис: </Text>
                    <View style={styles.textContainer}>
                        <Text style={styles.text}>{habit.description}</Text>
                    </View>

                    <Text>Час, коли я хочу отримувати сповіщення:</Text>
                    
                    <View style={{ flexDirection: 'row', flex: 1 , justifyContent: 'center',  alignItems: 'center' }}>
                        <View  style={{  flex: 1, alignItems: 'center' }}>
                            <Text>
                                {
                                    this.state.date.getHours().toString().length === 1 ? '0' +  this.state.date.getHours().toString() :  this.state.date.getHours().toString()
                                }
                                :
                                {
                                    this.state.date.getMinutes().toString().length === 1 ? '0' +  this.state.date.getMinutes().toString() :  this.state.date.getMinutes().toString()
                                }
                            </Text>
                        </View>
                        <View style={{  flex: 1, justifyContent: 'center'  }} >
                            <View>
                                <Button onPress={this.timepicker} title="Обрати час" />
                            </View>
                            { show && <DateTimePicker 
                                        value={date}
                                        mode={mode}
                                        is24Hour={true}
                                        display="default"
                                        onChange={this.setDate} />
                            }
                        </View>
                    </View>

                  

                    <Text>Дні, в які я хочу отримувати оповіщення про звичку: </Text>
                    <FlatList
                        data={this.state.checkedDays}
                        renderItem={({item, index}) => {
                            return (
                                <View style={styles.checkboxContainer}>
                                    <CheckBox
                                        value={this.state.checkedDays[index][0]}
                                        onChange={() => {
                                            const newCheckedDays = this.state.checkedDays;
                                            newCheckedDays[index][0] = !newCheckedDays[index][0]
                                            this.setState({checkedDays: newCheckedDays});
                                        }}
                                    />
                                    <Text>{item[1]}</Text>
                                </View>
                            )
                        }}
                        keyExtractor={item => item[1]}
                    />


                </View>
                <View style={styles.boxedButton}>
                    <Button style={styles.button} onPress={() => this.onSubmit()} title="Розвивати" />
                </View>
            </>
        );
    }

    async componentDidMount() {
        // We need to ask for Notification permissions for ios devices
        let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);

        if (Constants.isDevice && result.status === 'granted') {
            console.log('Notification permissions granted.')
        }

        // If we want to do something with the notification when the app
        // is active, we need to listen to notification events and 
        // handle them in a callback
        Notifications.addListener(this.handleNotification);
    }

    handleNotification() {
        //const num = Notifications.getBadgeNumberAsync();

    }

    async onSubmit() {
        //console.warn(new Date().getDay());
        if (new Date() > this.state.date || (new Date().getDay() === 0 && !this.state.checkedDays[6][0])) {
            return;
        }
        const { navigation } = this.props;
        const localNotification = {
            title: 'Слід ' +  this.habit.name.charAt(0).toLowerCase() + this.habit.name.slice(1),
            body: 'Не забудь виконати та пам\'ятай про штрафи!'
        };
        const date = this.state.date;
        date.setSeconds(0, 0);
        const schedulingOptions = {
            time: date.getTime()
        }

        // Notifications show only when app is not active.
        // (ie. another app being used or device's screen is locked)
        Notifications.scheduleLocalNotificationAsync(
            localNotification, schedulingOptions
        );
        const obj = {
            id: randomInteger(0, 34000),
            name: this.habit.name,
            description: this.habit.description,
            lastCompletedDate: null
        };
        let data = JSON.parse(await AsyncStorage.getItem('userHabits')) || [];
        data = [
            ...data,
            obj
        ];

        await AsyncStorage.setItem('userHabits', JSON.stringify(data));
        
        navigation.navigate('HabitsCategoriesList');
        //navigation.goBack();
    }

    setDate = (event, date) => {
        date = date || this.state.date;
    
        this.setState({
            show: Platform.OS === 'ios' ? true : false,
            date,
        });
    }
    
    show = mode => {
        this.setState({
          show: true,
          mode,
        });
    }
    
    datepicker = () => {
        this.show('date');
    }
    
    timepicker = () => {
        this.show('time');
    }
}

HabitScreen.navigationOptions = {
    title: 'Створення екозвички'
};

const styles = StyleSheet.create({
    boxed: {
        marginHorizontal: 10,
        marginVertical: 5,
        flex: 1
    },
    textContainer: {
        display: 'flex',
        alignItems: "center",
        padding: 5,
        marginBottom: 10,
        justifyContent: "center",
        fontSize: 100,
        borderColor: "black",
        borderRadius: 4,
        borderWidth: 1,
        borderStyle: "solid",
    },
    checkboxContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
      textAlign: "center"  
    },
    boxedButton: {
        marginHorizontal: 10,
        marginVertical: 5
    },
    button: {
        padding: 10
    }
});

function randomInteger(min, max) {
    let rand = min + Math.random() * (max - min);
    return Math.round(rand);
}
import React from 'react';
import {
    FlatList,
    StyleSheet,
    Button,
    View,
    AsyncStorage
} from 'react-native';

export default class SelectedHabitsListScreen extends React.Component {
    async componentDidMount() {
        const habitsString = await AsyncStorage.getItem('userHabits') || [];
        this.setState({
            userHabits: JSON.parse(habitsString),
            prevString: this.state.curString,
            curString: habitsString
        });
    }

    constructor(props) {
        super(props);
        this.state = {
            prevString: "",
            curString: "",
            userHabits: []
        }; 
        props.navigation.addListener('focus', async () => {
            const habitsString = await AsyncStorage.getItem('userHabits');
            if (this.state.curString != habitsString) {
                this.setState({
                    userHabits: JSON.parse(habitsString)
                });
            }
          });
    }

    render() {
        const { navigation } = this.props;
        return (
            <>
                <View style={styles.container}>
                        {this.state.userHabits && Array.isArray(this.state.userHabits) && this.state.userHabits.length > 0 &&
                            <FlatList
                                data={this.state.userHabits}
                                renderItem={({item}) => {
                                    return ( 
                                        <View style={styles.habitContainer}>
                                            <Button 
                                                title={item.name || ""}
                                                onPress={() => {
                                                    navigation.navigate('MySelectedHabit', {
                                                        habitId: item.id
                                                    })
                                                }}>
                                            </Button>
                                        </View>
                                    )
                                }}
                            keyExtractor={item => item.id + "*#$"}
                        />
                    }
                </View>
            </>
        );
    }
}
  
SelectedHabitsListScreen.navigationOptions = {
    title: 'Обрані екозвички'
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    habitContainer: {
        fontSize: 25,
        padding: 20,
    }
});


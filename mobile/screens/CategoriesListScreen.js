import React from 'react';
import {
    FlatList,
    StyleSheet,
    Button,
    View
} from 'react-native';

import categories from "./../data/categories";

export default class CategoriesListScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={categories}
                    renderItem={({item}) => {
                        return (
                            <View style={styles.categoryContainer}>
                                <Button
                                    title={item.name}
                                    onPress={() => {
                                        this.props.navigation.navigate('HabitsList', {
                                            categoryId: item.id
                                        })
                                    }}>
                                </Button>
                            </View>
                        )
                    }}
                    keyExtractor={item => item.id.toString() + "category"}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    categoryContainer: {
        fontSize: 25,
        padding: 20,
    }
});

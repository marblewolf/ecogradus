import React from 'react';
import {
    Button,
    Text,
    View,
    StyleSheet,
    AsyncStorage
} from 'react-native';

import { userProfile } from './../data/user-profile';

export default class SelectedHabitScreen extends React.Component {

    async componentDidMount() {
        const habitId = parseInt(this.props.route.params['habitId']);
        const habitsString = await AsyncStorage.getItem('userHabits') || [];
        const userHabits = JSON.parse(habitsString);
        const selectedHabit = userHabits.find(x => x.id === habitId);
        this.setState({
            userHabits: userHabits,
            selectedHabit: selectedHabit,
            isCompletedToday: this.getIsCompletedToday(selectedHabit)
        });
    }

    constructor(props) {
        super(props);
        this.state = {
            userHabits: null,
            selectedHabit: null,
            isCompletedToday: false,
        }; 
    }

    onHabitComplete = async(habit) => {
        habit.lastCompletedDate = Date.now();
        await AsyncStorage.setItem('userHabits', JSON.stringify(this.state.userHabits))
        userProfile.bonusQuantity += 50;
        this.setState({isCompletedToday: true});
    }

    onDelete = async(habitId) => {
        const newUserHabits = this.state.userHabits.filter((habit) => habit.id !== habitId);
        await AsyncStorage.setItem('userHabits', JSON.stringify(newUserHabits))
        this.props.navigation.popToTop();
    }

    getIsCompletedToday = (habit) => {
        if(!habit.lastCompletedDate){
            return false;
        }
        const diffTime = Math.abs(Date.now() - habit.lastCompletedDate);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
        return diffDays > 0;
    }

    render() {
        if(!this.state.selectedHabit) {
            return null;
        }
        const habit = this.state.selectedHabit;

        return (
            <>
                <View style={styles.boxed}>
                    <Text>Назва: </Text>
                    <View style={styles.textContainer}>
                        <Text style={styles.text}>{habit.name}</Text>
                    </View>
                    <Text>Опис: </Text>
                    <View style={styles.textContainer}>
                        <Text style={styles.text}>{habit.description}</Text>
                    </View>

                </View>
                <View style={styles.bottomButtonsContainer}>
                    <Button color='red' onPress={async() => await this.onDelete(habit.id)} title="Видалити" />
                    <Button 
                        style={styles.button} 
                        disabled={this.state.isCompletedToday}
                        onPress={async() => await this.onHabitComplete(habit)} title="Вже виконав сьогодні" 
                    />
                </View>

            </>
        );
    }
}

SelectedHabitScreen.navigationOptions = {
    title: 'Обрана екозвичка'
};

const styles = StyleSheet.create({
    boxed: {
        marginHorizontal: 10,
        marginVertical: 5,
        flex: 1
    },
    textContainer: {
        display: 'flex',
        alignItems: "center",
        padding: 5,
        marginBottom: 10,
        justifyContent: "center",
        fontSize: 100,
        borderColor: "black",
        borderRadius: 4,
        borderWidth: 1,
        borderStyle: "solid",
    },
    text: {
      textAlign: "center"  
    },
    bottomButtonsContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: "space-around",
        marginBottom: 15
    },
    button: {
        padding: 10
    }
});
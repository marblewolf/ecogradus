import React from 'react';
import {
    FlatList,
    StyleSheet,
    Button,
    View
} from 'react-native';

import categories from "./../data/categories";

export default class HabitsListScreen extends React.Component {
    render() {
        const { navigation, route } = this.props;
        const categoryId = parseInt(route.params['categoryId']);
        const selectedCategory = categories.find((category) => category.id == categoryId);
        return (
            <View style={styles.container}>
                <FlatList
                    data={selectedCategory.habits}
                    renderItem={({item}) => {
                        return ( 
                            <View style={styles.categoryContainer}>
                                <Button 
                                    title={item.name}
                                    onPress={() => {
                                        navigation.navigate('Habit', {
                                            categoryId: categoryId,
                                            habitId: item.id
                                        })
                                    }}>
                                </Button>
                            </View>
                        )
                    }}
                    keyExtractor={item => item.id.toString() + categoryId.toString() + "habit"}
                />
            </View>
        );
    }
}
  
HabitsListScreen.navigationOptions = {
    title: 'Екозвички'
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    categoryContainer: {
        fontSize: 25,
        padding: 20,
    }
});


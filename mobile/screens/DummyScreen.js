import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class DummyScreen extends React.Component {
    render() {
        return (
            <View style={{padding: 25}}>
                <View style={styles.textContainer}>
                    <Text style={styles.text}>Lorem ...</Text>
                </View> 
            </View>
        );
    }
}

  
const styles = StyleSheet.create({
    textContainer: {
        display: 'flex',
        alignItems: "center",
        padding: 5,
        marginBottom: 10,
        justifyContent: "center",
        fontSize: 100,
        borderColor: "black",
        borderRadius: 4,
        borderWidth: 1,
        borderStyle: "solid",
    },
    text: {
        textAlign: "center"  
    },
});

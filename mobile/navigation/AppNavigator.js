import React from 'react';
import { Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import TabBarIcon from '../components/TabBarIcon';
import CategoriesListScreen from '../screens/CategoriesListScreen';
import SettingsScreen from '../screens/SettingsScreen';
import HabitListScreen from '../screens/HabitsListScreen';
import HabitScreen from '../screens/HabitScreen';
import MyHabitsListScreen from '../screens/SelectedHabitsListScreen';
import MySelectedHabitScreen from '../screens/SelectedHabitScreen';
import Icon from 'react-native-vector-icons/Octicons';
import DummyScreen from '../screens/DummyScreen';


const MenuIcon = ({ navigation }) => <Icon 
    name='three-bars' 
    size={25} 
    color='#000'
    style={{padding: 20}}
    onPress={() => navigation.toggleDrawer()}
/>;

const HabitsStack = createStackNavigator();
function HabitsStackScreen() {
    return (
        <HabitsStack.Navigator>
            <HabitsStack.Screen 
                options={(props) => ({
                    title: 'Категорії екозвичок', 
                    headerLeft: () => MenuIcon(props)
                })}
                name="HabitsCategoriesList"
                component={CategoriesListScreen}
            />
            <HabitsStack.Screen options={{title: 'Екозвички'}} name="HabitsList" component={HabitListScreen} />
            <HabitsStack.Screen options={{title: 'Створення екозвички'}} name="Habit" component={HabitScreen} />
        </HabitsStack.Navigator>
    );
}
  
const MyHabitsStack = createStackNavigator();
function MyHabitsStackScreen() {
    return (
        <MyHabitsStack.Navigator>
            <MyHabitsStack.Screen
                options={(props) => ({
                    title: 'Обрані екозвички',
                    headerLeft: () => MenuIcon(props)
                })}
                name="MyHabitsList"
                component={MyHabitsListScreen}
            />
            <MyHabitsStack.Screen options={{title: 'Обрана екозвичка'}} name="MySelectedHabit" component={MySelectedHabitScreen} />
        </MyHabitsStack.Navigator>
    );
}
  
const SettingsStack = createStackNavigator();
function SettingsStackScreen() {
    return (
        <SettingsStack.Navigator>
            <SettingsStack.Screen
                options={(props) => ({
                    title: 'Профіль користувача',
                    headerLeft: () => MenuIcon(props)
                })}
                name="Settings"
                component={SettingsScreen}
            />
        </SettingsStack.Navigator>
    );
}

const DummyStack = createStackNavigator();
function DummyStackScreen() {
    return (
        <DummyStack.Navigator>
            <DummyStack.Screen
                options={(props) => ({
                    title: 'Here will be your title...',
                    headerLeft: () => MenuIcon(props)
                })}
                name="Dummy"
                component={DummyScreen}
            />
        </DummyStack.Navigator>
    );
}

function TabNavigator() {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused }) => {
                    let iconName;
                    switch (route.name) {
                        case "Habits": iconName = 'walk'; break;
                        case "MyHabits": iconName = 'star'; break;
                        case "Settings": iconName = 'person'; break;
                    }
                    return <TabBarIcon name={(Platform.OS === 'ios' ? 'ios-' : 'md-') + iconName} focused={focused} />;
                },
            })}
        >
            <Tab.Screen options={{title: "Екозвички"}} name="Habits" component={HabitsStackScreen} />
            <Tab.Screen options={{title: "Мої екозвички"}} name="MyHabits" component={MyHabitsStackScreen} />
            {/* <Tab.Screen options={{title: "Профіль"}} name="Settings" component={SettingsStackScreen} /> */}
        </Tab.Navigator>
    );
}

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
export default function createAppContainer() {
    return (
        <NavigationContainer>
            <Drawer.Navigator>
                <Drawer.Screen name="Home" options={{ drawerLabel: 'Екологічні звички' }} component={TabNavigator} />
                <Drawer.Screen name="Profile" options={{ drawerLabel: 'Мій профіль' }} component={SettingsStackScreen} />
                <Drawer.Screen name="Laws" options={{ drawerLabel: 'Екологічне законодавство' }} component={DummyStackScreen} />
                <Drawer.Screen name="Shops" options={{ drawerLabel: 'Магазини' }} component={DummyStackScreen} />
                <Drawer.Screen name="Settings" options={{ drawerLabel: 'Налаштування' }} component={DummyStackScreen} />
                <Drawer.Screen name="About" options={{ drawerLabel: 'Про ініціативу' }} component={DummyStackScreen} />
                <Drawer.Screen name="Help" options={{ drawerLabel: 'Допомога ініціативі' }} component={DummyStackScreen} />
            </Drawer.Navigator>
        </NavigationContainer>
    );
}
